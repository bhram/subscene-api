import axios from "axios";
import cheerio from "cheerio";

const BASE_URL = "https://subscene.com";
axios.defaults.baseURL = BASE_URL;

interface IResultType {
    title: string;
    count: string | number;
    url: string;
}
interface ISubtitleType {
    title: string;
    langauge: string ;
    isPositive: boolean;
    comment: string;
    url: string;
}

class Subscene {
    public async searchByTitle(title: string): Promise<IResultType[]> {
        const {data : html} = await axios.post("/subtitles/searchbytitle", {  query: title  });
        const $ = cheerio.load(html);
        const results: IResultType[] =  [];
        $(".byTitle ul li ").each((i, e) => {
            const item = $(e);
            const text  = item.find("a").text().trim();
            const count  = item.find(".count").text().match(/\d+/);
            const url = BASE_URL + item.find("a").attr("href");
            results.push({title: text, count: count ? count[0] : 0, url});
        });
        return results;
    }

    public async getSubtitles(url: string, langauge?: "Arabic" | "English"): Promise<ISubtitleType[]> {
        const {data : html} = await axios.get(url);
        const $ = cheerio.load(html);
        const results: ISubtitleType[] =  [];
        $("table tr").each((i, e) => {
            const item = $(e);
            const link = item.find(".a1 a").attr("href");
            if (link) {
                const title = item.find(".a1 span:nth-child(2)").text().trim().replace(/[\r\n]/g, "");
                const lan = item.find(".a1 span:nth-child(1)").text().trim();
                const isPositive = item.find("span:nth-child(1)").hasClass("positive-icon");
                const comment = item.find(".a6").text().trim();
                results.push({title, langauge: lan, isPositive, comment, url: BASE_URL + link});
            }
        });
        return langauge ? results.filter((r) => langauge.toLowerCase().includes(r.langauge.toLowerCase())) : results;
    }

    public async directLink(url: string): Promise<string> {
        const {data : html} = await axios.get(url);
        const $ = cheerio.load(html);
        return BASE_URL + $("div.download a").attr("href");
    }
}

export default new Subscene();
