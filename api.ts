import { Router } from "express";
import Subscene from "./Subscene";

const api = Router();

api.get("/search/:query", async (req, res) => {
    const query = req.params.query;
    if (!query) {
      res.send("query parameter is require");
    }
    const r = await Subscene.searchByTitle(query);
    res.send(r);
});

api.post("/subtitles", async (req, res) => {
    const link = req.body.link;
    if (!link) {
      res.send("link parameter is require");
    }
    const r = await Subscene.getSubtitles(link, req.body.langauge || "Arabic");
    res.send(r);
});
api.post("/download", async (req, res) => {
    const link = req.body.link;
    if (!link) {
      res.send("link parameter is require");
    }
    const r = await Subscene.directLink(link);
    res.send(r);
});

export default api;
