"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const cheerio_1 = __importDefault(require("cheerio"));
const BASE_URL = "https://subscene.com";
axios_1.default.defaults.baseURL = BASE_URL;
class Subscene {
    searchByTitle(title) {
        return __awaiter(this, void 0, void 0, function* () {
            const { data: html } = yield axios_1.default.post("/subtitles/searchbytitle", { query: title });
            const $ = cheerio_1.default.load(html);
            const results = [];
            $(".byTitle ul li ").each((i, e) => {
                const item = $(e);
                const text = item.find("a").text().trim();
                const count = item.find(".count").text().match(/\d+/);
                const url = BASE_URL + item.find("a").attr("href");
                results.push({ title: text, count: count ? count[0] : 0, url });
            });
            return results;
        });
    }
    getSubtitles(url, langauge) {
        return __awaiter(this, void 0, void 0, function* () {
            const { data: html } = yield axios_1.default.get(url);
            const $ = cheerio_1.default.load(html);
            const results = [];
            $("table tr").each((i, e) => {
                const item = $(e);
                const link = item.find(".a1 a").attr("href");
                if (link) {
                    const title = item.find(".a1 span:nth-child(2)").text().trim().replace(/[\r\n]/g, "");
                    const lan = item.find(".a1 span:nth-child(1)").text().trim();
                    const isPositive = item.find("span:nth-child(1)").hasClass("positive-icon");
                    const comment = item.find(".a6").text().trim();
                    results.push({ title, langauge: lan, isPositive, comment, url: BASE_URL + link });
                }
            });
            return langauge ? results.filter((r) => langauge.toLowerCase().includes(r.langauge.toLowerCase())) : results;
        });
    }
    directLink(url) {
        return __awaiter(this, void 0, void 0, function* () {
            const { data: html } = yield axios_1.default.get(url);
            const $ = cheerio_1.default.load(html);
            return BASE_URL + $("div.download a").attr("href");
        });
    }
}
exports.default = new Subscene();
//# sourceMappingURL=Subscene.js.map