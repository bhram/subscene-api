import bodyParser from "body-parser";
import express from "express";
import api from "./api";
import Subscene from "./Subscene";

const app = express();

const { PORT = 80 } = process.env;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/api", api);

app.listen(PORT, () => {
  console.log(`server started at http://localhost:${PORT}`);
});
